#run this shell script to kill all screen sessions at once
for session in $(screen -ls | grep -o '[0-9]\+')
do
	screen -S "${session}" -X quit;
done
