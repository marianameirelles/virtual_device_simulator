'''script to run device simulator script for each of the currently available devices in Tako002'''
import os
import json
import shlex
import subprocess
from string import Template
import argparse

PARSER = argparse.ArgumentParser(description="Send channel data from virtual devices to Tako server")
PARSER.add_argument("iterations", metavar='N', type=int,
                    help="number of times to send channel data. enter 0 for infinite run.")
OPTIONS = PARSER.parse_args()

# loops through the device list json
FNULL = open(os.devnull, 'w') # redirect subprocess stdout
DEV_JSON_FILE = open("seed_devices.js")
DEV_JSON_FILE = DEV_JSON_FILE.read()
JSON_OBJ = json.loads(DEV_JSON_FILE)
# run script in screen for each one
NUM_DEVICES = len(JSON_OBJ["dev"])

for device in range(0, NUM_DEVICES):
    dev_id = JSON_OBJ["dev"][device]["id"]
    auth_key = JSON_OBJ["dev"][device]["auth"]
    cmdTemplate = Template("screen -dmS $name python device_simulator.py -r -dev $dev_id -key $auth_key -n $iterations")
    cmdTemplate = cmdTemplate.substitute(name=dev_id, dev_id=dev_id, auth_key=auth_key,
                                             iterations=OPTIONS.iterations)
    cmd = shlex.split(cmdTemplate)
    process = subprocess.Popen(cmd, stdout=FNULL)
