"""this script can be used to send simulated data from a virtual device"""
import os
import subprocess
import json
from collections import OrderedDict
import random
import shlex
import time
import shutil
from string import Template
import parse_args

OPTIONS = parse_args.parse_arguments() #from parse_args.py

#DEBUG and RAND are either True or False
DEBUG = OPTIONS.debug
RAND = OPTIONS.rand
DOWN = False
if RAND:
    UP = False
else:
    UP = True
FNULL = open(os.devnull, 'w') #redirect subprocess stdout
SECS = OPTIONS.interval #seconds interval between readings
NUM = OPTIONS.num_iterations
DEV_ID = OPTIONS.dev_id
KEY = OPTIONS.auth_key

#4-20 sensor values
MAX = 0.0200
MIN = 0.0040

#set JSON encoder to save floats with 4 point precision
json.encoder.FLOAT_REPR = lambda f: ("%.4f" %f)
json.encoder.c_make_encoder = None

#setup function that copies over clean version of json file in case it was
#damaged in a previous execution
def setup():
    """clear json data file and copy over clean file"""
    clean = open("data_copy.json")
    clear = open("data.json", "w")
    shutil.copyfileobj(clean, clear)

def main():
    """opens json file and calls function to send channel data from each of the N
    channels"""
    if DEBUG:
        print "Starting simulator..."
        print ""
    setup()
    device_json_file = open("data.json")#open json file
    device_json = device_json_file.read()
    device_json_object = json.loads(device_json, object_pairs_hook=OrderedDict) #decode json object
    # set device id in json
    device_json_object['dev']['dev_id'] = DEV_ID;
    if UP:
        device_json_object = setup_increase(device_json_object)
    send_data(device_json_object)

    if DEBUG:
        print "Shutting down simulator..."

def setup_increase(device_json_object):
    num_channels = len(device_json_object['dev']['chans'])
    for ch_id in range(0, num_channels):
        device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = MIN
    return device_json_object

def invoke_executable():
    """invoke avmsavdc script"""
    if DEBUG:
        print "Invoking executable..."

    #call avmsa subprocess
    cmd = Template("./avmsavdc -s 64.40.116.246 -p 36360 -j data.json -i $dev_id -a $auth_key")
    cmd = cmd.substitute(dev_id=DEV_ID, auth_key=KEY)
    cmd_list = shlex.split(cmd)
    subprocess.Popen(cmd_list, stdout=FNULL)
    if DEBUG:
        print "Executable invoked"
        print ""

def increment_unit_data(device_json_object, ch_id):
    """increments unit data in ch_id of device_json_object by one unit and
    returns updated device_json_object"""
    if DEBUG:
        print "Incrementing data."

    #access current channel value
    ch_val = device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val']
    ch_val = min(ch_val + 0.0001, MAX)
    if ch_val == MAX:
        UP = not UP
        DOWN = not DOWN
    #save updated value
    device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = ch_val
    send_device_data(device_json_object)
    return device_json_object

def decrement_unit_data(device_json_object, ch_id):
    """decrements unit data in ch_id of device_json_object by one unit and
    returns updated device_json_object"""
    if DEBUG:
        print "Decrementing data."

    #access current channel value
    ch_val = device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val']
    ch_val= max(ch_val - 0.0001, MIN)
    if ch_val == MIN:
        UP = not UP
        DOWN = not DOWN
    #save updated value
    device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = ch_val
    send_device_data(device_json_object)
    return device_json_object

def generate_random_data(device_json_object, ch_id):
    """generates random data between 0.004 and 0.020 for a specific channel"""
    if DEBUG:
        print "Generating random data."
    #generate random number between 0 and 100
    rand_num = random.uniform(MIN, MAX)
    if DEBUG:
        print ""
        print ""
        print rand_num

    #save random number to json
    device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = rand_num
    send_device_data(device_json_object)
    return device_json_object

def send_device_data(device_json_object):
    """saves updated json with update values into data.json and calls function to
    executve avmsavdc command"""
    device_json_file = open("data.json", 'w')
    json.dump(device_json_object, device_json_file)
    invoke_executable() #call function to invoke

def inf_readings(device_json_object, current_time, num_channels):
    """sends data every SECS seconds infinitely"""
    # run infinitely
    while True:
        #call increment data method for each channel
        for ch_id in range(0, num_channels):
            device_json_object['dts'] = current_time #update DTS
            #call either random data or incremented or decremented data function
            if RAND:
                device_json_object = generate_random_data(device_json_object, ch_id)
            elif UP:
                increment_unit_data(device_json_object, ch_id)
            elif DOWN:
                decrement_unit_data(device_json_object, ch_id)
            time.sleep(10)
            current_time += SECS #increment time by SECS

def num_readings(device_json_object, current_time, num_channels):
    """sends data every SECS seconds for NUM times"""
    # run NUM times
    for _ in range(0, NUM):
        #call increment data method for each channel
        for ch_id in range(0, num_channels):
            device_json_object['dts'] = current_time #update DTS
            #call either random data or incremented or decremented data function
            if RAND:
                device_json_object = generate_random_data(device_json_object, ch_id)
            elif UP:
                increment_unit_data(device_json_object, ch_id)
            elif DOWN:
                decrement_unit_data(device_json_object, ch_id)
            time.sleep(10)
            current_time += SECS #increment time by SECS

def send_data(device_json_object):
    """calls functions for random, increasing, or decreasing data for each of the
    devices N channels and updates date time stamp according to the indicated
    second intervals"""
    #count number of current channels
    num_channels = len(device_json_object['dev']['chans'])
    if DEBUG:
        print "This device has " + str(num_channels) + " channels."
        print "Sending updated device data for each of these channels"
        print ""
    current_time = time.time()
    if NUM == 0:
        inf_readings(device_json_object, current_time, num_channels)
    else:
        num_readings(device_json_object, current_time, num_channels)

main()
