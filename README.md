# Virtual Device Simulator

Simulator that sends reading data from each of the devices channels to the Tako servers at user-defined intervals over the course of 24 hours (simulated) or for a given number of iterations. Written using Python 2.7.13.

### Installation 
Must be run on debian because the avmsavdc executable was built for this. The program relies on screen, which can be installed as such:
`apt-get install screen`
The programs uses the devices in seed_devices.js - you can edit seed_devices.js to add or remove virtual devices and auth keys as wanted. The default json file used is data_copy.json. At the beginning of each execution the contents of this file are copied over to data.json to ensure that the file being used by the scripts has not been corrupted by previous executions (this is done by the program). 

### Usage
#### Infinite Simulator for Multiple Devices
This program sends random 4-20 data to each channel of each of the virtual devices listed in seed_devices.js. 
To run:
`python multiple_run.py N`
Where N is an integer (number) argument that indicates the number of times to send data. If you want to send data infinitely (or until stopped), enter 0. The program will simulate a sensor reading every 15 seconds. 
Running this program will create multiple "screens," specifically, one for each of the devices in seed_devices.js. 
To kill all these screen at once, run the kill_screen shell script as such:
`./kill_screen.sh`
To check if the screens have been deleted:
`screen -list`
It should read "No Sockets found in [your device]"
multiple_run.py calls device_simulator.py for each of the given simulated devices. 

#### Simulator for a Specific Device
For testing purposes, it may be helpful to look at just one device at a time. 
To run:
`screen -dmS [dev_id] python device_simulator.py [-h] [-d] [-r] [-n NUM_ITERATIONS] [-i INTERVAL] -dev DEV_ID -key AUTH_KEY`
If you specify the flag -r the device will send random 4-20 sensor data. If not specified, then the device will send increasing data until the max is reached. Then it will send decreasing data until the min is reached, alternating between these two functionalities. Note that the flags in [brackets] are optional and do not need to be included to run. 
Here is an example of a call to run the simulator with random data, five times, at an interval of 60 seconds for device 00020000000000000000000000000006:
`screen -dmS 00020000000000000000000000000006 python device_simulator.py -r -n 5 -i 60 -dev 00020000000000000000000000000006 -key 70553e4c0b0b4d9f887af6f848d153d8`
For more information on how to run this program:
`python device_simulator.py -h`
This will display a list of the arguments and their descriptions.
#### Min-Max Simulator for a Specific Device
This program will only send two values, alternating between the min and max value a sensor could send. 
To run:
`screen -dmS DEV_ID python min_max.py -d DEV_ID -a AUTH_KEY`
This program runs infinitely. To exit the program use ctrl-c or use the appropriate screen command:
`screen -S NAME_OF_SCREEN_TO_DELETE -X quit`
To see the name of the screen use:
`screen -list`
Note that if the device you are using for the Min-Max simulator is already being used by one of the other programs, you should kill the other screens before starting this one, or else the device will be sending data from various different programs.
