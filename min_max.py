"""repeatedly sends min reading and max reading for a given device"""
import os
import subprocess
import json
from collections import OrderedDict
import random
import shlex
import time
import shutil
from string import Template
import argparse

parser = argparse.ArgumentParser(description="simulate alternating min and max sensor readings on device")
parser.add_argument("-d", default="00020000000000000000000000000004", help="id of device to simulate readings from")
parser.add_argument("-a", default="56318444b1a2497c9ac3509b2bb4fbd6", help="authentication key of device")
OPTIONS = parser.parse_args()

FNULL = open(os.devnull, 'w') #redirect subprocess stdout
DEV_ID = OPTIONS.d 
KEY = OPTIONS.a 
SECS = 15

#set JSON encoder to save floats with 4 point precision
json.encoder.FLOAT_REPR = lambda f: ("%.4f" %f)
json.encoder.c_make_encoder = None

#setup function that copies over clean version of json file in case it was
#damaged in a previous execution
def setup():
    """clear json data file and copy over clean file"""
    clean = open("data_copy.json")
    clear = open("data.json", "w")
    shutil.copyfileobj(clean, clear)

def main():
    """opens json file and calls function to send channel data from each of the N
    channels"""
    setup()
    device_json_file = open("data.json")#open json file
    device_json = device_json_file.read()
    device_json_object = json.loads(device_json, object_pairs_hook=OrderedDict) #decode json object
    # set device id in json
    device_json_object['dev']['dev_id'] = DEV_ID;
    send_data(device_json_object)

def invoke_executable():
    """invoke avmsavdc script"""
    #call avmsa subprocess
    cmd = Template("./avmsavdc -s 64.40.116.246 -p 36360 -j data.json -i $dev_id -a $auth_key")
    cmd = cmd.substitute(dev_id=DEV_ID, auth_key=KEY)
    cmd_list = shlex.split(cmd)
    subprocess.Popen(cmd_list, stdout=FNULL)

def min_unit_data(device_json_object, ch_id):
    device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = 0.004 
    send_device_data(device_json_object)
    return device_json_object

def max_unit_data(device_json_object, ch_id):
    device_json_object['dev']['chans'][ch_id]['ch_data'][0]['val'] = 0.020 
    send_device_data(device_json_object)
    return device_json_object

def send_device_data(device_json_object):
    """saves updated json with update values into data.json and calls function to
    executve avmsavdc command"""
    device_json_file = open("data.json", 'w')
    json.dump(device_json_object, device_json_file)
    invoke_executable() #call function to invoke

def send_data(device_json_object):
    """calls functions for random, increasing, or decreasing data for each of the
    devices N channels and updates date time stamp according to the indicated
    second intervals"""
    #count number of current channels
    num_channels = len(device_json_object['dev']['chans'])
    current_time = time.time() 
    # run infinitely
    while True:
        print("START LOOP")
        #call increment data method for each channel
        for ch_id in range(0, num_channels):
            print("channel")
            print(ch_id)
            device_json_object['dts'] = current_time #update DTS
            #call either random data or incremented or decremented data function
            device_json_object = max_unit_data(device_json_object, ch_id)
            time.sleep(10)
            device_json_object = min_unit_data(device_json_object, ch_id)
            time.sleep(10)
            current_time += SECS #increment time by SECS

main()
