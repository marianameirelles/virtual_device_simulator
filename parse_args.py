"""parse command line flags and return options"""
import argparse

def parse_arguments():
    """parse flags and return options"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", dest="debug", action="store_true", default=False,
                        help="sets debug to true so that debug messages are displayed in terminal")
    parser.add_argument("-r", dest="rand", action="store_true", default=False,
                        help="sets random to true so that data values are randomly generated for 4-20 sensor data")
    parser.add_argument("-n", dest="num_iterations", type=int, default=1,
                        help="number of times simulator will run")
    parser.add_argument("-i", dest="interval", type=int, default=15,
                        help="length of intervals, in seconds, between simulated readings")
    parser.add_argument("-dev", dest="dev_id", required=True, help="device ID")
    parser.add_argument("-key", dest="auth_key", required=True,
                        help="authentication key of chosen device")
    options = parser.parse_args()
    return options
